//HAshasuhsasasas
const express = require("express");
const { mySqlQuery } = require("./db");
var cors = require("cors");

const app = express();
app.use(cors());

const PORT = process.env.PORT || 3001;

app.get("/", (req, res) => {
  return res.send("test");
});
app.get("/Questions", async (req, res) => {
  const questions = await mySqlQuery(
    `SELECT * FROM questions WHERE answer >= :minYear AND answer <= :maxYear ORDER BY RAND() LIMIT :limit;`,
    {
      minYear: req.query.minYear,
      maxYear: req.query.maxYear,
      limit: parseInt(req.query.limit),
    }
  );
  console.log("Loading question:", req.query);
  return res.json(questions);
});
app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));
