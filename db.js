require("dotenv").config();

const SQL_PASSWORD = process.env.SQL_PASSWORD;

const host = process.env.PRODUCTION === "development" ? "localhost" : "heroku";

const config = {
  mySql: {
    development: {
      user: "root",
      password: SQL_PASSWORD,
      database: "historyguessr",
      host: host, //localhost
    },
  },
};
// Database
const mySql = require("mysql2/promise");
const mySqlPool = mySql.createPool(config.mySql.development);

let pool;

const startMySql = async () => {
  try {
    pool = await mySqlPool.getConnection();
    pool.connection.config.namedPlaceholders = true;
    console.log(
      `Initiated mySql with ${JSON.stringify(config.mySql.development)}`
    );
  } catch (e) {
    console.log(e);
  }
};

exports.mySqlQuery = async (query, params = {}) => {
  try {
    const [result] = await pool.query(query, params);
    return result;
  } catch (e) {
    // Ugly restart
    await startMySql();
    const [backupResult] = await pool.query(query, params);
    return backupResult;
  }
};

exports.startMySql = startMySql;
